# FluxCD Repo for starting cluster

### Install FluxCD

```shell
export GITLAB_TOKEN=<your-token>
flux bootstrap gitlab \
  --owner=castlecraft \
  --repository=fluxcd-starter \
  --branch=main \
  --path=clusters/production \
  --token-auth \
  --reconcile
```

### Configure

- clusters/production/flux-system/gotk-sync.yaml: Change gitlab url
- apps/production/erpnext-backup-cronjob.yaml: Change s3 credentials
- apps/production/erpnext-values.yaml: Change Redis and MariaDB hosts
- apps/production/kustomization.yaml: Change ingress file
- apps/production/s3-secret.yaml: Change Secret Key
- apps/production/erp-example-com-ingress.yaml: Rename
- infrastructure/nfs-provisioner/deployment.yaml: Change NFS Host
- resources/cluster-issuer.yaml: Change letsencrypt email

### Local k3d based testing

For k3d use `--network host`

```shell
 k3d cluster create devcluster \
  --api-port 127.0.0.1:6443 \
  -p 80:80@loadbalancer \
  -p 443:443@loadbalancer \
  --k3s-server-arg "--no-deploy=traefik" \
  --network host
```

Start MinIO, MariaDB, Redis and NFS Services

```shell
# Start NFS
sudo systemctl start nfs-server # change as per linux distro

# Start MinIO
export MINIO_ACCESS_KEY="AKIAIOSFODNN7EXAMPLE"
export MINIO_SECRET_KEY="wJalrXUtnFEMI/K7MDENG"

docker run -d --name minio \
  -e "MINIO_ACCESS_KEY=$MINIO_ACCESS_KEY" \
  -e "MINIO_SECRET_KEY=$MINIO_SECRET_KEY" \
  -p "9000:9000" \
  minio/minio server /data

s3cmd --access_key=$MINIO_ACCESS_KEY \
  --secret_key=$MINIO_SECRET_KEY \
  --region=us-east-1 \
  --no-ssl \
  --host=localhost:9000 \
  --host-bucket=localhost:9000 \
  mb s3://bucket-name

# Start MariaDB
docker run --name mariadb \
  -e MYSQL_ROOT_PASSWORD=admin \
  -p "3306:3306" \
  -d mariadb:10.3 --character-set-server=utf8mb4 --collation-server=utf8mb4_unicode_ci

# Start Redis
docker run --name redis -p "6379:6379" -d redis:alpine
```
